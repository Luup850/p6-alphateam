import sys
import os
import datetime
from tensorflow.python.keras.backend import conv1d
from tensorflow.python.keras.engine.input_layer import Input, InputLayer
sys.path.insert(0, os.path.abspath(os.path.dirname(__file__) + "/PythonModules")) 
import numpy as np
import TrajectoryConstruction as TC
import tensorflow as tf
import matplotlib.pyplot as plt
import random
from sklearn.model_selection import train_test_split
class CNN():
    _training_label = []
    _training_data = []
    _test_data = []
    _test_label = []

    #[
    #    [1,2,3,4,5]
    #    [1,2,3,4,5]
    #    [1,2,3,4,5]
    #    [1,2,3,4,5]
    #    [1,2,3,4,5]
    #    [1,2,3,4,5]
    #    [1,2,3,4,5]
    #    [1,2,3,4,5]
    #    [1,2,3,4,5]
    #    [1,2,3,4,5]
    #]


    _model = tf.keras.Sequential([
            #tf.keras.layers.Flatten(input_shape=(11, 5)),
            #tf.keras.layers.Input(shape=(11,5)),
            tf.keras.layers.Conv1D(5, 5, input_shape=(11, 5), activation='relu'),
            #tf.keras.layers.Conv1D(5, 3, input_shape=(7, 5), activation='relu'),
            tf.keras.layers.Flatten(input_shape=(7, 5)),
            tf.keras.layers.Dense(32, activation='relu'),
            tf.keras.layers.Dense(2, activation='softmax')
        ])
    
    def load_training_data(self, trainingDataLocation):
        '''
        Input is a filepath of the training data
        The function will then load this data, label the data points and calling the function that chooses the test set
        '''
        #Uses the filepath minus the file name, followed by the name of the training data file.
        #trainingDataLocation = __file__[:-21]+"TrainingSegments.npy"

        data = np.load(trainingDataLocation, allow_pickle=True)
        print("Number of segments:", len(data))
        for d in data:
            temp_list = []

            if d[1]:
                self._training_label.append(1)
            else:
                self._training_label.append(0)

            for f in d[0]:
                temp_list.append([f.distance, f.speed, f.acceleration, f.jerk, f.course])

            self._training_data.append(temp_list)

        self._get_testset()    

    def _get_testset(self):
        self._training_data, self._test_data, self._training_label, self._test_label = train_test_split(self._training_data, self._training_label, test_size=0.33)        
   
    def compile(self):
        self._model.compile(optimizer='adam',
            loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
            metrics=['accuracy'])

    def train(self, log=False, number_epochs=400):
        log_dir = "logs/fit/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
        _tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)
        if(log):
            self._model.fit(self._training_data, self._training_label, epochs=number_epochs, callbacks=[_tensorboard_callback])
        else:
            self._model.fit(self._training_data, self._training_label, epochs=number_epochs, callbacks=[_tensorboard_callback])

    def evaluate(self):
        test_loss, test_acc = self._model.evaluate(self._test_data,  self._test_label)
        print('\nTest accuracy:', test_acc)
        self._model.summary()

    def save_weights(self, filepath):
        self._model.save_weights(filepath)

    def load_weights(self, filepath):
        self._model.load_weights(filepath)

    def predict(self, data, simplify_output=True):
        '''
        Input is the data set and the function will output a tuple of the activity of a neuron. If simplify_output is true, 
        it will simplify the activity to either 1 or 0.
        '''

        tmp = []
        for d in data:
            tmp_trajectory = []
            for j in d[0]:
                tmp_trajectory.append([j.distance, j.speed, j.acceleration, j.jerk, j.course])
            
            tmp.append(tmp_trajectory)
        
        #Output is a list of predictions for each segment. A single segment would look like this: [neuron0_val, neuron1_val]
        #Simplify output simplifies such that the output of each segment would be 1 or 0, where 0 is not fishing, and 1 is fishing.
        if simplify_output: 
            return np.argmax(self._model.predict(tmp, batch_size=len(tmp)), axis = 1)
        else:
            return self._model.predict(tmp, batch_size=len(tmp))

    def debug(self):
        temp = np.argmax(self._model.predict(self._test_data, batch_size=len(self._test_data)), axis = 1)
        for i in range(len(self._test_label)):
            print("Test:", (self._test_label)[i], " - ", temp[i])

    def score(self):
        r = tf.keras.metrics.Recall()
        p = tf.keras.metrics.Precision()
        predictions = np.argmax(self._model.predict(self._test_data, batch_size=len(self._test_data)), axis = 1)

        r.update_state(predictions, self._test_label)
        p.update_state(predictions, self._test_label)
        print("Recall:", r.result().numpy())
        print("Precision:", p.result().numpy())
        print("F1 score:", (2 * ((p.result().numpy() * r.result().numpy()) / (p.result().numpy() + r.result().numpy()))))




#Used for testing:
#s = CNN()
#s.load_training_data(__file__[:-21]+"TrainingSegments.npy")
#s.predict(s._test_data)
#s.compile()
#s.train(number_epochs=400, log=True)
#print(s.predict(s._test_data, simplify_output=True))
#s.debug()
#print(np.argmax(s.predict(), axis=1))
#s.load_weights(__file__[:-21] + "CNNModels/test.h5")
#s.score()
#s.evaluate()
#s.save_weights(__file__[:-21] + "CNNModels/test.h5")