# P6: Identifying Individual VesselBehavior Via a Modified FishNet
To start the server, follow the steps below:

1. Go to `/p6-alphateam/web/SmallTest` and insert AIS data in .csv format.
2. Start the server via the included script  (LinuxStartServer.sh for linux and WindowsStartServer.bat for windows).
3. The server should now be running on `http://localhost:5000/`.
4. Go to the website and click on the button `Go` (Alternatively go to `http://localhost:5000/show` if a map has already been generated).
5. The server will now generate a map over suspicious vessels. Depending on the amount of data, this may take some time so be patient.
6. The server should now be hosting the map. Go to `http://localhost:5000/show` to see it.

## Dependencies
Python 3.8.5 and the following dependencies are needed for the system to run:

tensorflow 2.4.1, matplotlib 3.3.4, scikit-learn 0.24.2, cartopy, Flask 1.1.2, Werkzeug 1.0.1


### Setup Conda

1. Create new Conda enviroment with tensorflow (conda create -n tensorflow tensorflow python=3.8.5)
2. Activate the enviroment (conda Activate tensorflow)
3. Forge cartopy into enviroment (conda install -c conda-forge cartopy)
4. start the server in the enviroment


