#%%
import os
from flask import Flask, render_template, request, jsonify
from flask.helpers import url_for
from werkzeug.utils import redirect, secure_filename
from werkzeug.datastructures import ImmutableMultiDict
import requests

BASE = "http://127.0.0.1:1234/"

app = Flask(__name__)

UPLOAD_FOLDER = os.path.abspath(os.path.dirname(__file__))[:-3] + "web/Data"
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ["csv"]

@app.route("/", methods=["POST","GET"])
def Home():
    return render_template("Frontpage.html")

@app.route("/render", methods=["POST","GET"])
def Render():
    
    data = request.args['data_select']
    #print((jsonify(data)))
    print(data)
    Response = requests.post(BASE + "getfile/" + data)
    if Response.status_code == 200:
        return render_template("Render.html")
    else:
        return {data : 400}

@app.route("/Show")
def show():
    return render_template("Render.html")


if __name__ == "__main__":
    app.run()

 # %%
