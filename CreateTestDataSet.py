#%%
from datetime import datetime
import sys
import os
import numpy as np
sys.path.insert(0, os.path.abspath(os.path.dirname(__file__) + "/PythonModules"))
import TrajectoryConstruction as TC
import data_extractor as DE
import AISDataCleaner as ADC
import Segment as S
import ImportAISDataFromCSV as impcsv
import pandas as pd
import csv
#%%
def __main__():
#%%
    Training = DE.extract(__file__[:-21]+"/Training", 0)
    AISlist = []
    TClist = []
    print(len(Training))
    print(Training[0])
#%%
    for T in Training:
        AISlist.append(TC.AISPoint(T[0],T[1],T[4],T[5],T[6],T[7],T[8])) 

    AISlist.sort(key =lambda x : x.time)
    AISlist.sort(key = lambda x : x.mmsi)
    
    AISlist = ADC.RemoveUnusablePoints(AISlist)
    print(len(AISlist))
    TClist = TC.GenerateTrajectories(AISlist, True)
    
    Segments = S.segment_trajectories(TClist,11)
    FalseFishers = []
    NotFishers = []
    for x in Segments:
        if(x[1]):
            FalseFishers.append(x[0])
        else:
            NotFishers.append(x[0])
#%%

    Output = [("TimeStamp","Type of mobile", "MMSI", "Latitude", "Longitude", "Navigational status", "Rot", "SOG", "COG", "Heading", "IMO", "Callsign", "Name", "Ship type")]
#%%
    for i in range(10):
        for x in NotFishers[i]:
            t = x.Start
            timeb = datetime.utcfromtimestamp(t.time-1).strftime("%d/%m/%Y %H:%M:%S")
            timea = datetime.utcfromtimestamp(t.time+1).strftime("%d/%m/%Y %H:%M:%S")
            Output.append((timeb,"Class A", t.mmsi, t.lat, t.long, "Under TEST conditions", 0, t.sog, t.cog, 0, 0, "testing SHIP", "SS testing", "Royal navy"))
            Output.append((timea,"Class A", t.mmsi, t.lat, t.long, "Under TEST conditions", 0, t.sog, t.cog, 0, 0, "testing SHIP", "SS testing", "Royal navy"))
        
        t = NotFishers[i][-1].End
        timeb = datetime.utcfromtimestamp(t.time-1).strftime("%d/%m/%Y %H:%M:%S")
        timea = datetime.utcfromtimestamp(t.time+1).strftime("%d/%m/%Y %H:%M:%S")
        Output.append((timeb,"Class A", t.mmsi, t.lat, t.long, "Under TEST conditions", 0, t.sog, t.cog, 0, 0, "testing SHIP", "SS testing", "Royal navy"))
        Output.append((timea,"Class A", t.mmsi, t.lat, t.long, "Under TEST conditions", 0, t.sog, t.cog, 0, 0, "testing SHIP", "SS testing", "Royal navy"))
    for x in FalseFishers[0]:
        t = x.Start
        timeb = datetime.utcfromtimestamp(t.time-1).strftime("%d/%m/%Y %H:%M:%S")
        timea = datetime.utcfromtimestamp(t.time+1).strftime("%d/%m/%Y %H:%M:%S")
        Output.append((timeb,"Class A", t.mmsi, t.lat, t.long, "Under TEST conditions", 0, t.sog, t.cog, 0, 0, "testing SHIP", "SS testing", "Royal navy"))
        Output.append((timea,"Class A", t.mmsi, t.lat, t.long, "Under TEST conditions", 0, t.sog, t.cog, 0, 0, "testing SHIP", "SS testing", "Royal navy"))
    t = FalseFishers[0][-1].End
    timeb = datetime.utcfromtimestamp(t.time-1).strftime("%d/%m/%Y %H:%M:%S")
    timea = datetime.utcfromtimestamp(t.time+1).strftime("%d/%m/%Y %H:%M:%S")
    Output.append((timeb,"Class A", t.mmsi, t.lat, t.long, "Under TEST conditions", 0, t.sog, t.cog, 0, 0, "testing SHIP", "SS testing", "Royal navy"))
    Output.append((timea,"Class A", t.mmsi, t.lat, t.long, "Under TEST conditions", 0, t.sog, t.cog, 0, 0, "testing SHIP", "SS testing", "Royal navy"))    
#%%
    with open('TestData.csv','w+') as out:
        csv_out=csv.writer(out)
        csv_out.writerows(Output)

#%%
__main__()
# %%
