#%%
import sys
import os
main_folder = (os.path.abspath(os.path.dirname(__file__)))
import numpy as np
sys.path.insert(0, main_folder + "/PythonModules")
import TrajectoryConstruction as TC
import data_extractor as DE
import AISDataCleaner as ADC
import Segment as S
import ImportAISDataFromCSV as impcsv
main_folder = (os.path.abspath(os.path.dirname(__file__)))
def __main__():

    Training = DE.extract(main_folder+"/Training", 0)
    AISlist = []
    TClist = []
    print(len(Training))


    for T in Training:
        AISlist.append(TC.AISPoint(T[0],T[1],T[4],T[5],T[6],T[7],T[8])) 
    
    AISGeneratedData = impcsv.GenerateDataFromCSVFolder(main_folder+"/RawAIS")
    for x in AISGeneratedData:
        AISlist.append(x)
    
    AISlist.sort(key =lambda x : x.time)
    AISlist.sort(key = lambda x : x.mmsi)
    
    AISlist = ADC.RemoveUnusablePoints(AISlist)
    print(len(AISlist))
    TClist = TC.GenerateTrajectories(AISlist)
    
    Segments = S.segment_trajectories(TClist,11)
    print(len(Segments))
    np.save(main_folder+"/TrainingSegments", Segments)
    print("Marcus er fra københavn")


__main__()
# %%
