import requests
import time

url = 'https://www.marinetraffic.com/en/search/searchAsset?what=vessel&term='
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36',
}

def isFishingVessel(mmsi):
    temp_url = url + str(mmsi)
    print(temp_url)
    time.sleep(5)
    x = requests.get(temp_url, headers=headers)
    
    try:
        if(x.json()[0]["type_color"] == '2'):
            return True
        else:
            return False
    except:
        print("Exception: Couldnt find vessel!")
        return False