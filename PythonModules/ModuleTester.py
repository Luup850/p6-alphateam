
#%%

import TrajectoryConstruction as TC

x = TC.Derive
print(x(10,5,10,0))
c1 = TC.AISPoint(1234,0, 15, 45, 55, 60, True)
c2 = TC.AISPoint(1234,10, 18, 45, 57, 60, True)
c3 = TC.AISPoint(1234,20, 20, 45, 70, 60, True)
print(TC.CalcDistance(c1, c2))

t1 = TC.Trajectory(c1,c2)

dataPoints = [c1,c2,c3]

trajectories = TC.GenerateTrajectories(dataPoints)

for x in trajectories:
    print(x.distance, x.speed, x.acceleration, x.jerk, x. course, x.isFishing )
# %%

