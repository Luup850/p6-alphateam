import sys
import os
import numpy as np
main_folder = (os.path.abspath(os.path.dirname(__file__)))[:-14]
sys.path.insert(0, main_folder + "/PythonModules")
sys.path.insert(0, main_folder)
import TrajectoryConstruction as TC
import data_extractor as DE
import AISDataCleaner as ADC
import Segment as S
from TrainNeuralNetwork import CNN

Training = DE.extract("D:/TrainingData", 0)
AISlist = []
TClist = []

for T in Training:
    AISlist.append(TC.AISPoint(T[0],T[1],T[4],T[5],T[6],T[7],T[8]))

AISlist.sort(key =lambda x : x.time)
AISlist.sort(key = lambda x : x.mmsi)

AISlist = ADC.RemoveUnusablePoints(AISlist)
TClist = TC.GenerateTrajectories(AISlist)

Segments = S.segment_trajectories(TClist,11)
cnn = CNN()
cnn.compile()
cnn.load_weights(main_folder + "/CNNModels/test.h5")

labels = []
actual_segments = []
for s in Segments:
    labels.append(s[1])
    actual_segments.append(s[0])


prediction = cnn.predict(Segments)
#print(prediction)
correct = 0
wrong = 0
for i in range(len(labels)):
    if (int(labels[i]) == prediction[i]):
        correct = correct + 1
    else:
        wrong = wrong + 1

result = correct / (len(labels) / 100)
nresult = wrong / (len(labels) / 100)

print("Correct: {0} | Wrong: {1}".format(result, nresult))