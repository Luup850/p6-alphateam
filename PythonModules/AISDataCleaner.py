from datetime import time
from numpy.core.einsumfunc import _parse_possible_contraction
import TrajectoryConstruction as TC


def RemoveUnusablePoints(AISPoints):

    AISPoints.sort(key =lambda x : x.time)
    AISPoints.sort(key = lambda x : x.mmsi)
    tempPoints = []
    output = []
    canUsePrevPoint = 0
    for i in range (len(AISPoints)-1 ):
        # Checks that both datapoints belongs to the same ship
        if (AISPoints[i].mmsi != AISPoints[i+1].mmsi): 
            tempPoints = []
            canUsePrevPoint = 0
        # Checks that the points exist on the same side of a 10 min interval
        elif((AISPoints[i].time - (AISPoints[i].time % 600)) == (AISPoints[i+1].time - (AISPoints[i+1].time % 600)) ):
            #skip to the next points if this is not the case
            continue
        else:
            #generate an average point, that might be used for trajectories
            tempPoint = AveragePoint(AISPoints[i], AISPoints[i+1])
            if(len(tempPoints) == 0):   
                tempPoints.append(tempPoint)
            #This should not be neccesary due to the list being sorted earlier, left here just in case
            #elif(AISPoints[i].time <= tempPoints[-1].time):
                #continue
            else:
                #If the points does not create a 10 min interval, start over from the newest point
                if((( tempPoint.time - (tempPoints[-1].time)) != 600) ):
                    tempPoints = []
                    canUsePrevPoint = 0
                tempPoints.append(tempPoint)
            
            #When the desired amount of consecutive datapoints have been reached, move them to output
            if(len(tempPoints) == (12-canUsePrevPoint)):
                for x in tempPoints:
                    output.append(x)
                tempPoints = []
                canUsePrevPoint = 1

    return output

def CalculateAverageLongitude(currentLong, previousLong, timeCurrent, timePrevious):
    ChangeInLong = CorrectLongitude(currentLong - previousLong)

    time = int(timeCurrent - timeCurrent % 600)
    ChangeInLong = previousLong+(ChangeInLong/(timeCurrent-timePrevious)*(time-timePrevious))
    ChangeInLong = CorrectLongitude(ChangeInLong)
    return ChangeInLong
    
def AveragePoint(point1, point2):
    mmsi = point1.mmsi
    time = int(point2.time - point2.time % 600)  # % 600 works by rounding to nearest 10 min
    sog = ValueAtTimeBetweenPoints(point1.sog, point2.sog, point1.time, point2.time, time )
    cog = ValueAtTimeBetweenPoints(point1.cog, point2.cog, point1.time, point2.time, time )
    lat = ValueAtTimeBetweenPoints(point1.lat, point2.lat, point1.time, point2.time, time ) 
    long =  CalculateAverageLongitude(point2.long,point1.long,point2.time, point1.time)
    isFishing = point1.isFishing
    output = TC.AISPoint(mmsi, time, sog, cog, lat , long, isFishing)
    #Det her er sgu nok dårlig code, skal vi gøre noget ved det ?
    if(hasattr(point1, 'Status')):
        output.Status = point1.Status
        output.Type = point1.Type
    return output

def ValueAtTimeBetweenPoints(ValueAtFirstPoint, ValueAtSecondPoint, TimeAtFirstPoint, TimeAtSecondPoint, Time ):
    TimeFromFirstPointToTime = (Time - TimeAtFirstPoint)
    ValueAtTime = ValueAtFirstPoint + TC.Derive(ValueAtSecondPoint,ValueAtFirstPoint,TimeAtSecondPoint ,TimeAtFirstPoint) * TimeFromFirstPointToTime
    return ValueAtTime

#Ensures that the longitude does not go over 180 or under -180
def CorrectLongitude(longitude):
    if (longitude < -180 ):
        longitude += 360
    elif(longitude > 180 ):
        longitude -= 360
    return longitude

