#%%
import sys
import os
import cartopy.crs as ccrs
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
import matplotlib.pyplot as plt
import math




def PlotCords(ShipSeg):
    print("i am plotting now")
    
    
    RGB = ["blue", "green", "yellow", "red"]
    # Blue Non fishing ship, Green Fishing ship no sus, Yellow fishing vessel but not broadcasting, Red not a fishing ship and not broadcasting
    #TestPoints = [[1, 5.5, 57, 1], [1, 5.60, 54, 1], [1, 5.7, 56, 1], [1, 5.8, 58, 1 ], [1, 5.9, 55,0],[1, 6.0, 54.5,0], [1, 6.1, 55, 0]]

    plt.figure(figsize=(18, 12), dpi = 300)
    map = plt.axes(projection=ccrs.PlateCarree())
    map.set_extent([0, 20, 53, 60], ccrs.PlateCarree()) 
    #map.set_extent([-175, -180, -12.5, -22.5], ccrs.PlateCarree())
    grid_lines = map.gridlines(draw_labels=True)
    grid_lines.xformatter = LONGITUDE_FORMATTER
    grid_lines.yformatter = LATITUDE_FORMATTER
    map.coastlines()


    lastpoint = {}
    for i in ShipSeg:
        if i[3] != 0:   
            map.scatter(i[1], i[2], s= 10, color= RGB[i[3]] ,transform=ccrs.PlateCarree())
            if(i[0] in lastpoint.keys()):
                tempCords = lastpoint.get(i[0])
                distance = math.sqrt( ((i[1]-tempCords[0])**2)+((i[2]-tempCords[1])**2) )
                if distance < 1 :
                    plt.plot([tempCords[0] , i[1]], [tempCords[1], i[2]],
                        color= RGB[i[3]], linewidth=2, marker='o',
                        transform=ccrs.Geodetic(),
                        )

    lastpoint = {i[0]: [i[1], i[2]] }
    print("saving File")
    plt.savefig(os.path.abspath(os.path.dirname(__file__))[:-14] + "/web/static/Datapoint2.png")
    print("Done saving")


#debug print
#print(os.path.abspath(os.path.dirname(__file__))[:-14]+"/web/static/Datapoint1.png")
#print(os.path.abspath(os.path.dirname(__file__))+"Second debug print")
#PlotCords(TestPoints)
#ShipSeg = AISWEB.ClassifyAISData(Simplify=False, Cutoff=0.90)
#ShipSeg = AISWEB.ClassifyAISData()
#print("plotting now")
#PlotCords(ShipSeg)



# %%
