import math
import datetime
# Cordinate class to keep track of what cordinate contains
class AISPoint():
    '''
    Is a ais datapoint, containing all the data neccesary to generate trajectories
    Requires the following inputs in order 
    (mmsi, time, sog, cog, lat, long , isFishing) Where is fishing is a label
    '''
    def __init__(self, mmsi, time, sog, cog, lat, long, isFishing):
        self.mmsi = mmsi
        self.time = int(time)
        self.sog = sog
        self.cog = cog
        self.lat = lat
        self.long = long
        self.isFishing = isFishing
# Trajectory class to keep track a trajectory Cannot be generated without 2 AIs input
class Trajectory():
    '''
    Contains everything needed for use in the CNN, including label
    '''
    def __init__(self, c1 , c2, prevAcceleration = 0):
        if(c1.time == c2.time):
            raise ValueError("Time for two points in trajectory have to be different")
        self.distance = CalcDistance(c1,c2)
        self.speed = c2.sog
        
        self.acceleration = Derive(self.speed, c1.sog, c2.time, c1.time)
        self.jerk =  Derive(self.acceleration, prevAcceleration , c2.time, c1.time)
        self.course = Derive(c2.cog, c1.cog, c2.time, c1.time)
        self.isFishing = c1.isFishing
def GenerateTrajectories (AISPoints, SavePoints = False):
    '''
    Takes a list of AISPoints and converts it into trajectories  
    '''
    trajectories = list()
    prevAcceleration = 0
    for i in range (len(AISPoints)-1 ) :
        if((AISPoints[i].mmsi != AISPoints[i+1].mmsi) or (AISPoints[i].time + 600 != AISPoints[i+1].time)):
            prevAcceleration = 0
        else:
            tempTrajectory = Trajectory(AISPoints[i], AISPoints[i+1],prevAcceleration)
            if(SavePoints):
                tempTrajectory.Start = AISPoints[i]
                tempTrajectory.End = AISPoints[i+1]
            trajectories.append(tempTrajectory)
            prevAcceleration = tempTrajectory.acceleration
    return trajectories

 
def Derive (currentValue, previousValue, timeCurrent, timePrevious):
    '''
    Calculates average change between two values per unit of time 
    Input(currentValue, previousValue, timeCurrent, timeprevious)
    '''
    #print(currentValue, previousValue)
    return ((currentValue - previousValue) / (timeCurrent - timePrevious))

#Calculates distance between two cordinates and return it in Km
def CalcDistance (cordinate1, cordinate2):
    '''
    Uses the haversine formula to calculate the amount of Km between two cordinates
    '''
    #Earth radius is the average radius of the earth
    earth_radius = 6371
    lat1 = ConvertToradian(cordinate1.lat)
    long1 = ConvertToradian(cordinate1.long)
    lat2 = ConvertToradian(cordinate2.lat)
    long2 = ConvertToradian(cordinate2.long)
    delta_lat = lat2-lat1
    delta_long = long2-long1
    sin_to_lat = math.pow(math.sin((delta_lat)/2),2)
    cos_to_lat = math.cos(lat1)*math.cos(lat2)
    sin_to_long = math.pow(math.sin(delta_long/2),2)
    a = math.sqrt(sin_to_lat+cos_to_lat*sin_to_long)
    distance = 2*earth_radius*math.asin(a)
    
    return distance

def ConvertToradian (degree):
    '''
    Converts from degree to radian
    '''
    return degree * math.pi / 180

