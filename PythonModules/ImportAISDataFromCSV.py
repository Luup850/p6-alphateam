#%%
import sys
import os
from numpy.core.numeric import NaN
from numpy.lib.function_base import append
from numpy import loadtxt
import pandas as pd
import TrajectoryConstruction as TC
main_folder = (os.path.abspath(os.path.dirname(__file__)))
import AISDataCleaner as AC
import datetime as dt
from os import walk



def GenerateDataFromCSVFolder (folderpath, whitelist = True, MaxFiles = 0 ):
    files = []
    output = []
    
    for (dirpath, dirnames, filenames) in walk(folderpath):
        files.extend(filenames)
    count = 1
    for f in files:
        
        print( folderpath, f)
        data = pd.read_csv(folderpath + '/' + f)

        if(whitelist):
            nonFishingShips = loadtxt(main_folder+'/nonFishingShips.txt', delimiter=',',dtype=int)
            filterSeries = data.MMSI.isin(nonFishingShips)
            nonFishingDF = data[filterSeries]
            data = nonFishingDF
        filteredDF = data[(data.Longitude != 181) & (data.Latitude != 91)]
        filteredDF = filteredDF[filteredDF[['SOG', 'COG']].notnull().all(1)]
        for x in filteredDF.values:
            time = dt.datetime.strptime(x[0], "%d/%m/%Y %H:%M:%S")
            output.append(TC.AISPoint(x[2], dt.datetime.timestamp(time), x[7], x[8], x[3], x[4], False))
            output[-1].Status = x[5] # 'Engaged in fishing' is the only interesting status
            output[-1].Type = x[13]  # 'Fishing' is the only interesting type
        if (MaxFiles != 0 and count == MaxFiles):
            break
        else:
            count += 1
            
    print(len(output))

    return output


    
#GenerateDataFromCSVFolder(__file__[:-37] + 'RawAIS', False)
#data = pd.read_csv(__file__[:-37] + 'RawAIS/aisdk_20201001.csv')
#print(data.values)
