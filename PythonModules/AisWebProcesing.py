
from numpy import core
from tensorflow.python.ops.gen_math_ops import floor
import ImportAISDataFromCSV as AISCSV
import TrajectoryConstruction as TC
import AISDataCleaner as ADC
import Segment as S
import sys
import os
main_folder = (os.path.abspath(os.path.dirname(__file__))[:-14])
sys.path.insert(0, main_folder)
import TrainNeuralNetwork as TNN
import ship_checker as SC


# Format for tuples in the return list (MMSI, Longitude, Latitude, Status )
# Status 0 = no fishing, nothing to report
# Status 1 = Fishing,  broadcasting its fishing
# status 2 = Fishing, Not broadcasting, and a fishing ship
# status 3 = Fishing, Not broadcasting, Not a fishing ship
def ClassifyAISData (AisFile = main_folder + '/RawAIS', Simplify = True, Cutoff = 0.51, MaxFiles = 0, FilterResults = False ):
    CNN = TNN.CNN()
    CNN.load_weights(main_folder +'/CNNModels/test.h5')
    CNN.compile()
    output = []
    print(AisFile)
    AisPoints = AISCSV.GenerateDataFromCSVFolder(AisFile, False, MaxFiles)
    AisPoints = ADC.RemoveUnusablePoints(AisPoints)
    print(AisPoints[1].Status,(AisPoints[1].Type ))
    Trajectories = TC.GenerateTrajectories(AisPoints, True)
    print(Trajectories[0].Start.Status, Trajectories[0].Start.Type, Trajectories[0].Start.long, Trajectories[0].Start.lat)
    segments = S.segment_trajectories(Trajectories, 11)
    predictions = []
    if (Simplify):
        predictions = CNN.predict(segments, True)
    else:
        predictionTuples = CNN.predict(segments, False)
        for x in predictionTuples:
            if(x[1] >= Cutoff ):
                predictions.append(1)
            else:
                predictions.append(0)
    print(predictions)
    #STATS FOR DEBUGGING
    nonFishingTrue = 0
    NonFishingFalse = 0
    FishingTrue = 0
    FishingNotDeclared = 0
    FishingSuspicious = 0
    SusMMSIList = []
    ListOfFishingShips = []
    ListOfNonFishingShips = []
    for i in range (len(segments)):
        for trajectory in segments[i][0]:
            Status = 0
            if (predictions[i] == 1):
                if(trajectory.Start.Status == 'Engaged in fishing'):
                    Status = 1
                    FishingTrue += 1
                elif (trajectory.Start.Type == 'Fishing'):
                    Status = 2
                    FishingNotDeclared += 1
                else:
                    Status = 3
                    SusMMSIList.append(trajectory.Start.mmsi)
                    FishingSuspicious += 1       
            elif (predictions[i] == 0):
                if(trajectory.Start.Status == 'Engaged in fishing'):
                    NonFishingFalse += 1
                else:
                    nonFishingTrue += 1   
            if(Status == 3):

                if (trajectory.Start.mmsi in ListOfNonFishingShips):
                    Status = 0
                    output.append((trajectory.Start.mmsi, trajectory.Start.long, trajectory.Start.lat, Status))
                    #Shortcircuiting prevents lookup in case we already checked the ship
                elif( trajectory.Start.mmsi in ListOfFishingShips or SC.isFishingVessel(trajectory.Start.mmsi)):
                    output.append((trajectory.Start.mmsi, trajectory.Start.long, trajectory.Start.lat, Status))
                    ListOfFishingShips.append(trajectory.Start.mmsi)
                else:
                    Status = 0
                    output.append((trajectory.Start.mmsi, trajectory.Start.long, trajectory.Start.lat, Status))
                    ListOfNonFishingShips.append(trajectory.Start.mmsi)
            else:        
                output.append((trajectory.Start.mmsi, trajectory.Start.long, trajectory.Start.lat, Status))
    amount = len(output)
    print(amount)
    if(FilterResults):
        SusMMSIList = list(filter(SC.isFishingVessel, (set(SusMMSIList))))
    else:
        SusMMSIList = set(SusMMSIList)
    
    #print(output[0])
    print("Here the numbers be")
    print("Detected as nonfishing and not saying anything about fishing\t" + str(nonFishingTrue)+'='+ str(nonFishingTrue/ amount*100))
    print("Detected as nonfishing, but the say they are fishing\t"+ str(NonFishingFalse)+'='+ str(NonFishingFalse / amount*100))
    print("Detected as fishing, and saying that they are fishing\t"+str(FishingTrue)+'='+ str(FishingTrue/ amount*100))
    print("Detected as Fishing, and they are fishing ships\t"+str(FishingNotDeclared)+ '=' + str(FishingNotDeclared/ amount*100))
    print("Detected as fishing, but not saying anything about fishing\t" + str(FishingSuspicious)+'='+ str(FishingSuspicious/ amount*100))
    print("Suspicious MMSI below")
    print(SusMMSIList)
    return(output)
        
def Percentage(num1, num2):

    return str(floor((num1/num2)*100))
    
#ClassifyAISData()
#ClassifyAISData(__file__[:-32] + 'RawAIS')



