#%%
import os
from flask import Flask, render_template, request
from flask.helpers import url_for
from werkzeug.utils import redirect, secure_filename
from flask_restful import Resource, Api
(os.path.abspath(os.path.dirname(__file__))[:-14])
import AisWebProcesing as AISWEB
import PlotMap as PM


app = Flask(__name__)

api = Api(app)

class Get_File(Resource):
    def post(self, name):
        print(name)
        if name != "":
            shipseg = AISWEB.ClassifyAISData((os.path.abspath(os.path.dirname(__file__))[:-14])  + "/web/Data/" + name, FilterResults = False)
            PM.PlotCords(shipseg)
            return {name : 200}
        else:
            return {name : 400}


api.add_resource(Get_File, "/getfile/<string:name>")

if __name__ == "__main__":
    app.run(host='127.0.0.1', port=1234)
# %%
