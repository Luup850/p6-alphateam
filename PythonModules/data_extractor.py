#%%
#(0:'mmsi', 1:'timestamp', 2:'distance_from_shore', 3:'distance_from_port', 
# 4:'speed', 5:'course', 6:'lat', 7:'lon', 8:'is_fishing')
import numpy as np
from os import walk

#Leave amount to 0 for all datapoints
#Returns an array of tuples containing the data
#
#Specific layout of each tuple in the output array
#(0:'mmsi', 1:'timestamp', 2:'distance_from_shore', 3:'distance_from_port', 
# 4:'speed', 5:'course', 6:'lat', 7:'lon', 8:'is_fishing')
def extract(folderpath, amount):
    '''
    Output is a tuple of following format
    (0:'mmsi', 1:'timestamp', 2:'distance_from_shore', 3:'distance_from_port', 
    4:'speed', 5:'course', 6:'lat', 7:'lon', 8:'is_fishing')
    '''
    files = []
    output = []
    for (dirpath, dirnames, filenames) in walk(folderpath):
        files.extend(filenames)

    for f in files:
        print(f)
        x = np.load(folderpath + '/' + f, allow_pickle=True)
        
        for a in x[x.files[0]]:
            
            if amount != 0 and len(output) == amount:
                break
            if(a[8] != -1 ):
                output.append(a)
    return output