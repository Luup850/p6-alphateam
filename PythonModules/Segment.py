import numpy as np

def segment_trajectories(trajectories, size):
    '''
        Input is a list of trajectories and a specefic size
        Output is a list of segments that contains 'size' amount of trajectories
    '''
    segments = []
    IsFishing = 0
    tempAray = []
    label = False
    for x in trajectories:
        if(len(tempAray) == size):
            if(IsFishing > 0 ):
                label = True
            segments.append(np.array((tempAray, label),dtype="object"))
            tempAray = []
            label = False
            IsFishing = 0
        tempAray.append(x)
        # Keep tracks of how many trajectories are fishing or nonfishing
        if(x.isFishing > 0):
            IsFishing += 1
        else:
            IsFishing -= 1
        

    return segments